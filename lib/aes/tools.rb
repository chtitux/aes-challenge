module Aes
  class Tools

    def self.key_to_bytes(key)
      key.scan(/../).map { |byte| byte.to_i(16) }
    end

    def self.bytes_to_key(bytes)
      bytes.map { |byte| '%02x' % byte }.join
    end

    def self.rot_word(word)
      word[1..-1] + [word[0]]
    end

    def self.sub_word(word)
      word.map do |byte|
        Aes::SBOX[byte]
      end
    end

    def self.rcon(word)
      [Aes::RCON[word], 0, 0, 0]
    end

    def self.xor_arrays(*words)
      result = words[0]

      words[1..-1].each do |word|
        result = (0..result.length-1).map {|index| result[index] ^ word[index] }
      end

      result
    end
  end
end
