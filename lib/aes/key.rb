module Aes
  class Key
    attr_reader :round_key, :round

    def initialize(round_key, round = 1)
      if round_key.is_a?(String)
        round_key = Aes::Tools.key_to_bytes(round_key)
      end

      @round_key = round_key
      @round = round
    end

    def next
      first_word = word_at(1)

      last_word = word_at(4)
      rot_word = Aes::Tools.rot_word(last_word)
      sub_word = Aes::Tools.sub_word(rot_word)

      rcon_word = Aes::Tools.rcon(round)

      new_word = Aes::Tools.xor_arrays(sub_word, first_word, rcon_word)
      next_key = new_word

      3.times do |n|
        new_word = Aes::Tools.xor_arrays(word_at(2+n), new_word)
        next_key += new_word
      end

      self.class.new(next_key, round + 1)
    end

    def to_s
      Aes::Tools.bytes_to_key(round_key)
    end

    def ==(o)
      o.class == self.class && o.state == state
    end

    def state
      [round, round_key]
    end

    private
    attr_writer :round_key, :round

    def word_at(n)
      round_key[4*(n-1) .. 4*n - 1]
    end

  end
end
