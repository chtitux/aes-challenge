module Aes
  class State
    attr_reader :state

    def initialize(state = nil, text: nil, key: nil)
      if !text.nil?
        state = text.unpack("c*")
      end

      @state = state
      @key = key
    end

    def to_s
      box.map do |row|
        row.map { |byte| '%02x' % byte }.join(' ')
      end.join("\n")
    end

    def box
      [
        [state[0], state[4], state[8],  state[12]],
        [state[1], state[5], state[9],  state[13]],
        [state[2], state[6], state[10], state[14]],
        [state[3], state[7], state[11], state[15]],
      ]
    end

    def encrypt!
      # pre-whitening
      add_round_key!

      # 9 full rounds (AES-128)
      9.times do
        sub_bytes!
        shift_rows!
        mix_columns!
        add_round_key!
      end

      # Last round
      sub_bytes!
      shift_rows!
      add_round_key!
    end

    def ==(o)
      o.class == self.class && o.state_array == state_array
    end

    def state_array
      [state, key]
    end

    private
    attr_writer :state
    attr_accessor :key

    def sub_bytes!
      @state = Aes::Tools.sub_word(state)
    end

    def shift_rows!
      @state = [
        state[0],  state[5],  state[10], state[15],
        state[4],  state[9],  state[14], state[3],
        state[8],  state[13], state[2],  state[7],
        state[12], state[1],  state[6],  state[11],
      ]
    end

    def shift_rows_inverse!
      @state = [
        state[0],  state[13], state[10], state[7],
        state[4],  state[1],  state[14], state[11],
        state[8],  state[5],  state[2],  state[15],
        state[12], state[9],  state[6],  state[3],
      ]
    end

    def mix_columns!
      mix_matrix = [
        [2, 3, 1, 1],
        [1, 2, 3, 1],
        [1, 1, 2, 3],
        [3, 1, 1, 2],
      ]

      @state = mix_column(column_at(1), mix_matrix) +
               mix_column(column_at(2), mix_matrix) +
               mix_column(column_at(3), mix_matrix) +
               mix_column(column_at(4), mix_matrix)
    end


    def mix_columns_inverse!
      mix_matrix = [
        [14, 11, 13,  9],
        [9,  14, 11, 13],
        [13,  9, 14, 11],
        [11, 13, 9,  14],
      ]

      @state = mix_column(column_at(1), mix_matrix) +
               mix_column(column_at(2), mix_matrix) +
               mix_column(column_at(3), mix_matrix) +
               mix_column(column_at(4), mix_matrix)
    end

    def add_round_key!
      @state = Aes::Tools.xor_arrays(state, key.round_key)
      @key = key.next
    end

    def add_round_key_inverse!
      add_round_key!
    end

    def mix_column(column, mix_matrix)
      [
        matrix_addition(column, mix_matrix[0]),
        matrix_addition(column, mix_matrix[1]),
        matrix_addition(column, mix_matrix[2]),
        matrix_addition(column, mix_matrix[3]),
      ]
    end

    def matrix_addition(column, factors)
      factors.each_with_index.map do |factor, index|
        value = column[index]

        case factor
        when 1
          value
        else
          Aes::MULTIPLICATIONS[factor][value]
        end
      end.reduce(:^)
    end

    def column_at(n)
      state[4*(n-1) .. 4*n - 1]
    end

  end
end
