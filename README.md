# Ruby implementation of the [AES tutorial](https://davidwong.fr/blockbreakers/aes.html)

[![build status](https://gitlab.com/chtitux/aes-challenge/badges/master/build.svg)](https://gitlab.com/chtitux/aes-challenge/pipelines)
[![coverage report](https://gitlab.com/chtitux/aes-challenge/badges/master/coverage.svg)](https://chtitux.gitlab.io/aes-challenge/coverage/)

This project is a Ruby implementation of the [AES tutorial](https://davidwong.fr/blockbreakers/aes.html) by David Wong.

## Unit Tests
This project is covered by unit tests. The *minimum* coverage is **100%**.
It means the [CI](https://gitlab.com/chtitux/aes-challenge/pipelines) will fail if one line of code is not run during tests.

If you want to contribute, please ensure the coverage won't be below 100%.

To run tests, simply run
~~~bash
make test
~~~

You can browse [coverage stats](https://chtitux.gitlab.io/aes-challenge/coverage/rcov/) online.

## Installation
Requirements: [ruby](https://www.ruby-lang.org/) 2.6.3.

~~~bash
make install
~~~

## Usage
Please have in mind this project **MUST NOT** be used in production, nor in any serious situation.

~~~ruby
require 'aes'
key = Aes::Key.new('d6aa74fdd2af72fadaa678f1d6ab76fe')
state = Aes::State(text: 'theblockbreakers', key: round_key)
state.encrypt!
# Results are stored in a box
state.box
~~~

Decryption is not done yet.

## Warranty
Do **not** use this library in production. It's for educational purpose only.

## Author
[Théophile Helleboid](https://theophile.re/)

## License
This project is licensed under [MIT License](LICENSE.md).
