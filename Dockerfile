FROM ruby:2.6.3

# Let's use a dedicated folder and drop privileges
WORKDIR /home/aes-challenge/
RUN useradd aes-challenge && chown aes-challenge /home/aes-challenge/
USER aes-challenge

# Install bunlder and gems
COPY Makefile Gemfile Gemfile.lock ./
RUN make install

# Copy the project
COPY . .

CMD make test
