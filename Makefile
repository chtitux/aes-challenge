test:
	bundle exec rake spec

console:
	bundle exec bin/console

docker-build:
	docker build -t aes-challenge .

docker-run:
	docker run aes-challenge

# Install or update the specified dependencies
install:
	@command -v ruby >/dev/null 2>&1 || { echo >&2 "I require ruby $$(cat .ruby-version) but it's not installed. Aborting."; exit 1; }
	@bundle -v >/dev/null 2>&1 || gem install bundler
	bundle install
