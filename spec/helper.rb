require "simplecov"
require "simplecov-rcov"
SimpleCov.formatters = [
  SimpleCov::Formatter::HTMLFormatter,
  SimpleCov::Formatter::RcovFormatter
]
SimpleCov.start do
  add_filter "spec/"
  minimum_coverage 100
end

require "rspec"
require "rspec/its"
require "pry"

# Load libs
require 'aes'
