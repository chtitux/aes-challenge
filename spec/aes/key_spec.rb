require "helper"

describe Aes::Key do
  describe "#next" do
    it 'returns the corresponding next key' do
      key = Aes::Key.new('2b7e151628aed2a6abf7158809cf4f3c')

      expect(key = key.next).to eq(Aes::Key.new('a0fafe1788542cb123a339392a6c7605', 2))
      expect(key = key.next).to eq(Aes::Key.new('f2c295f27a96b9435935807a7359f67f', 3))
      expect(key = key.next).to eq(Aes::Key.new('3d80477d4716fe3e1e237e446d7a883b', 4))
      expect(key = key.next).to eq(Aes::Key.new('ef44a541a8525b7fb671253bdb0bad00', 5))
      expect(key = key.next).to eq(Aes::Key.new('d4d1c6f87c839d87caf2b8bc11f915bc', 6))
      expect(key = key.next).to eq(Aes::Key.new('6d88a37a110b3efddbf98641ca0093fd', 7))
      expect(key = key.next).to eq(Aes::Key.new('4e54f70e5f5fc9f384a64fb24ea6dc4f', 8))
      expect(key = key.next).to eq(Aes::Key.new('ead27321b58dbad2312bf5607f8d292f', 9))
      expect(key = key.next).to eq(Aes::Key.new('ac7766f319fadc2128d12941575c006e', 10))
      expect(key = key.next).to eq(Aes::Key.new('d014f9a8c9ee2589e13f0cc8b6630ca6', 11))
    end
  end

  describe "#to_s" do
    it 'returns the corresponding next key' do
      key = Aes::Key.new('2b7e151628aed2a6abf7158809cf4f3c')

      expect(key.to_s).to eq('2b7e151628aed2a6abf7158809cf4f3c')
    end
  end
end
