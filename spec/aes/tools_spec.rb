require "helper"

describe Aes::Tools do
  describe ".key_to_bytes" do
    it 'returns the corresponding array of bytes' do
      expect(Aes::Tools.key_to_bytes('2b7e151628aed2a6abf7158809cf4f3c')).to eq([
        0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6,
        0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c,
      ])
    end
  end

  describe ".bytes_to_key" do
    it 'returns the corresponding string value' do
      expect(Aes::Tools.bytes_to_key([
        0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6,
        0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c,
      ])).to eq('2b7e151628aed2a6abf7158809cf4f3c')
    end
  end

  describe "#rot_word" do
    it 'rotates input without changing the input' do
      expect(Aes::Tools.rot_word([0x00, 0x01, 0x02, 0x03])).to eq([0x01, 0x02, 0x03, 0x00])
    end
  end

  describe "#sub_word" do
    it 'returns the corresponding sub word' do
      expect(Aes::Tools.sub_word([0x01, 0xc2, 0x9e, 0x00])).to eq([0x7c, 0x25, 0x0b, 0x63])
    end
  end

  describe "#rcon" do
    it 'returns the corresponding rcon value' do
      expect(Aes::Tools.rcon(0x01)).to eq([0x01, 0x00, 0x00, 0x00])
      expect(Aes::Tools.rcon(0x02)).to eq([0x02, 0x00, 0x00, 0x00])
      expect(Aes::Tools.rcon(0x03)).to eq([0x04, 0x00, 0x00, 0x00])
      expect(Aes::Tools.rcon(0x04)).to eq([0x08, 0x00, 0x00, 0x00])
    end
  end
end
