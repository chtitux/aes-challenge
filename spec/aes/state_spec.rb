require "helper"

describe Aes::State do
  let(:example_state) { (0..15).to_a }

  describe "#box" do
    it 'returns the state formatted in a box' do
      state = Aes::State.new(example_state)
      expect(state.box).to eq(
        [
          [ 0x00, 0x04, 0x08, 0x0c ],
          [ 0x01, 0x05, 0x09, 0x0d ],
          [ 0x02, 0x06, 0x0a, 0x0e ],
          [ 0x03, 0x07, 0x0b, 0x0f ],
        ]
      )

      # Documentation example
      state = Aes::State.new(text: 'this is one text')
      expect(state.box).to eq(
        [
          [ 0x74, 0x20, 0x6f, 0x74 ],
          [ 0x68, 0x69, 0x6e, 0x65 ],
          [ 0x69, 0x73, 0x65, 0x78 ],
          [ 0x73, 0x20, 0x20, 0x74 ],
        ]
      )
    end
  end

  describe "#to_s" do
    it 'returns the corresponding state in a box form with hexadecimal values' do
      state = Aes::State.new(example_state)
      expect(state.to_s).to eq(
        "00 04 08 0c\n" +
        "01 05 09 0d\n" +
        "02 06 0a 0e\n" +
        "03 07 0b 0f"
      )

      # Documentation example
      state = Aes::State.new(text: 'this is one text')
      expect(state.to_s).to eq(
        "74 20 6f 74\n"+
        "68 69 6e 65\n"+
        "69 73 65 78\n"+
        "73 20 20 74"
      )
    end
  end

  describe "#sub_bytes!" do
    it 'maps the bytes' do
      state = Aes::State.new(example_state)
      state.send(:sub_bytes!)

      expect(state.box).to eq([
        [ 0x63, 0xf2, 0x30, 0xfe ],
        [ 0x7c, 0x6b, 0x01, 0xd7 ],
        [ 0x77, 0x6f, 0x67, 0xab ],
        [ 0x7b, 0xc5, 0x2b, 0x76 ],
      ])
    end
  end

  describe "#shift_rows!" do
    it 'shifts the bytes' do
      state = Aes::State.new(example_state)
      state.send(:shift_rows!)

      expect(state.box).to eq([
        [ 0x00, 0x04, 0x08, 0x0c ],
        [ 0x05, 0x09, 0x0d, 0x01 ],
        [ 0x0a, 0x0e, 0x02, 0x06 ],
        [ 0x0f, 0x03, 0x07, 0x0b ],
      ])

      # Documentation example
      state = Aes::State.new(Aes::Tools.key_to_bytes('637c777bf26b6fc53001672Bfed7ab76'))
      state.send(:shift_rows!)

      expect(state.box).to eq([
        [ 0x63, 0xf2, 0x30, 0xfe ],
        [ 0x6b, 0x01, 0xd7, 0x7c ],
        [ 0x67, 0xab, 0x77, 0x6f ],
        [ 0x76, 0x7b, 0xc5, 0x2b ],
      ])
    end
  end

  describe "#shift_rows_inverse!" do
    it 'shits inverse the bytes' do
      state = Aes::State.new(example_state)
      state.send(:shift_rows_inverse!)

      expect(state.box).to eq([
        [ 0x00, 0x04, 0x08, 0x0c ],
        [ 0x0d, 0x01, 0x05, 0x09 ],
        [ 0x0a, 0x0e, 0x02, 0x06 ],
        [ 0x07, 0x0b, 0x0f, 0x03 ],
      ])

      # Documentation example to shift and unshift bytes
      reference_state = Aes::State.new(Aes::Tools.key_to_bytes('637c777bf26b6fc53001672Bfed7ab76'))
      state = reference_state.dup
      state.send(:shift_rows!)
      state.send(:shift_rows_inverse!)

      expect(state).to eq(reference_state)
    end
  end

  describe "#mix_columns!" do
    it 'mixes the columns' do
      state = Aes::State.new(example_state)
      state.send(:mix_columns!)

      expect(state.box).to eq([
        [0x02, 0x06, 0x0a, 0x0e],
        [0x07, 0x03, 0x0f, 0x0b],
        [0x00, 0x04, 0x08, 0x0c],
        [0x05, 0x01, 0x0d, 0x09],
      ])

      # Documentation example
      state = Aes::State.new(Aes::Tools.key_to_bytes('636b6776f201ab7b30d777c5fe7c6f2b'))
      state.send(:mix_columns!)

      expect(state.box).to eq([
        [ 0x6a, 0x2c, 0xb0, 0x27 ],
        [ 0x6a, 0x6d, 0xd9, 0x9c ],
        [ 0x5c, 0x33, 0x5d, 0x21 ],
        [ 0x45, 0x51, 0x61, 0x5c ],
      ])
    end
  end

  describe "#mix_columns_inverse!" do
    let(:state) { Aes::State.new(example_state) }

    it 'reverses the mix of the columns' do
      reference_state = Aes::State.new(example_state)
      state = reference_state.dup
      state.send(:mix_columns_inverse!)

      expect(state.box).to eq([
        [0x0a, 0x0e, 0x02, 0x06],
        [0x0f, 0x0b, 0x07, 0x03],
        [0x08, 0x0c, 0x00, 0x04],
        [0x0d, 0x09, 0x05, 0x01],
      ])
      state.send(:mix_columns!)
      expect(state).to eq(reference_state)

      # Documentation example applied on both mixing and reverse mixing
      reference_state = Aes::State.new(Aes::Tools.key_to_bytes('636b6776f201ab7b30d777c5fe7c6f2b'))
      state = reference_state.dup
      state.send(:mix_columns!)
      state.send(:mix_columns_inverse!)

      expect(state).to eq(reference_state)
    end
  end

  describe "#add_round_key!" do
    let(:round_key) { Aes::Key.new('d6aa74fdd2af72fadaa678f1d6ab76fe') }

    it 'adds a round key to the current state' do
      # Documentation example
      state = Aes::State.new(Aes::Tools.key_to_bytes('6a6a5c452c6d3351b0d95d61279c215c'), key: round_key)
      state.send(:add_round_key!)

      expect(state.box).to eq([
        [ 0xbc, 0xfe, 0x6a, 0xf1 ],
        [ 0xc0, 0xc2, 0x7f, 0x37 ],
        [ 0x28, 0x41, 0x25, 0x57 ],
        [ 0xb8, 0xab, 0x90, 0xa2 ],
      ])
    end
  end

  describe "#add_round_key_inverse!" do
    let(:round_key) { Aes::Key.new('d6aa74fdd2af72fadaa678f1d6ab76fe') }

    it 'adds inverse the round key' do
      # Documentation example applied to add and inverse add the round key
      reference_state = Aes::State.new(Aes::Tools.key_to_bytes('6a6a5c452c6d3351b0d95d61279c215c'), key: round_key)
      state = reference_state.dup
      state.send(:add_round_key!)

      new_state = Aes::State.new(state.state, key: round_key)
      new_state.send(:add_round_key_inverse!)
      # Equality can't be used as the round of the key in new_key equals 2
      expect(new_state.state).to eq(reference_state.state)
    end
  end

  describe "#encrypt!" do
    let(:round_key) { Aes::Key.new('2b7e151628aed2a6abf7158809cf4f3c') }

    it 'encrypt' do
      # Documentation example
      state = Aes::State.new(text: 'theblockbreakers', key: round_key)
      state.encrypt!

      expect(state.box).to eq([
        [ 0xc6, 0x02, 0x23, 0x2f, ],
        [ 0x9f, 0x5a, 0x93, 0x05, ],
        [ 0x25, 0x9e, 0xf6, 0xb7, ],
        [ 0xd0, 0xf3, 0x3e, 0x47, ],
      ])
    end
  end
end
